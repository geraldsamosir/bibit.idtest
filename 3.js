

const string = 'aaa (bbb) ccc (ddd)'


// the function to refactor
function findFirstStringInBracket(str){
    if(str.length > 0){
          let indexFirstBracketFound = str.indexOf("(");
          if(indexFirstBracketFound >= 0){
           let wordsAfterFirstBracket = str.substr( indexFirstBracketFound );
           if(wordsAfterFirstBracket){
              wordsAfterFirstBracket = wordsAfterFirstBracket.substr(1);
              let indexClosingBracketFound = wordsAfterFirstBracket.indexOf(")");
              if(indexClosingBracketFound >= 0){
                  return wordsAfterFirstBracket.substring(0, indexClosingBracketFound);
              }
              else{
                  return '';
              }
           }else{
              return '';
           }
       }else{
           return '';
       }
    }else{
       return '';
    }
 }
 

// refactor function
const findFirstStringInBracketRefactor(str)=>{
    const indexFirstBracketFound = str.indexOf("(");
    const newStr = str.substr(indexFirstBracketFound)
    const indexClosingBracketFound = newStr.indexOf(")");
    if(indexClosingBracketFound !== 0 && indexFirstBracketFound !==0){  
      return newStr.substring(1  , indexClosingBracketFound );
    } else {
      return ''
    }
}

console.log(findFirstStringInBracket(string))
console.log(findFirstStringInBracketRefactor(string))

 