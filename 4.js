let data = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']


const bubbleSort =(arr)=>{
   let len = arr.length;
   for (let i = len-1; i>=0; i--){
     for(var j = 1; j<=i; j++){
        if(arr[j-1]>arr[j]){
           let temp = arr[j-1];
           arr[j-1] = arr[j];
           arr[j] = temp;
        }
     }
   }
   return arr;
}

const groupAnagrams =(strArr) => {
  let result = {};
  for (let word of strArr) {
    let sortedWord = bubbleSort( word.split("")).join("");
    if (result[sortedWord]) {
      result[sortedWord].push(word);
    } else {
      result[sortedWord] = [word];
    }
  }
  return Object.values(result);
}

const orderbyLength = (arr) =>{
  let len = arr.length;
  for (let i = len-1; i>=0; i--){
    for(var j = 1; j<=i; j++){
       if(arr[j-1].length<arr[j].length){
          let temp = arr[j-1];
          arr[j-1] = arr[j];
          arr[j] = temp;
       }
       if(arr[j-1].length === arr[j].length){
         if(arr[j-1][0].length < arr[j][0].length)
          temp = arr[j-1];
          arr[j-1] = arr[j];
          arr[j] = temp;
       }
    }
  }
}

data = groupAnagrams(data);
orderbyLength(data)
const result = data;
console.log('result', result)