<h1>The Task</h1>
Please write a small ExpressJS server to search movies from http://www.omdbapi.com/ 
The Backend should  :
- Have 2 endpoint named  "/search" with GET method and "/detail" with GET method (single movie detail)
- Each API calls should be logged into a MySQL table, specifying DateTime, API endpoint getting called and the parameters passed
- Contain access credential to call the API as such : 
Key : "faf7e5bb&s"
URL : http://www.omdbapi.com/
  * Example url call to search is --> GET http://www.omdbapi.com/?apikey=faf7e5bb&s=Batman&page=2
- Be written in ExpressJS framework
Important aspects :
- Readability of code
- Good display on the knowledge of "Separation of Concerns for Codes"
- Write unit tests on some of the important files. Bigger plus points for complete unit test cases
- Good use of asynchronousy
Plus points:
- Deploy your result to a public URL so we can check the result
- Implementation of Clean Architecture is a BIG plus
- Complete Unit tests


<h1>Information for deployment</h1>
<ol>
<li>make sure database Mysql already at least version support json data type</li>
<li>make sure db name omdapi already exsist or you can make based your own db name and change config.json and docker file</li>
<li>if using docker to deploy all the configuration has declarated in docker file if want to change can change in Dockerfile</li>
<li>if manualy deploy just change config.json for configuration</li>
<li>to up model for database migration npm run migrateup and make sure the database has ready</li>
<li>to undo model migration npm run migrateundo</li>
<li>for api docs using swagger in http://localhost:3000/docs
    username: admin
    password: admin</li>
<li>for run the unit test just run npm run test</li>
</ol>
