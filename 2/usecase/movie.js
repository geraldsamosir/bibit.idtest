const rp = require('request-promise');
const config = require('../config');

class Movie {
  constructor(){
    this.SearchMovie = this.SearchMovie.bind(this);
    this.DetailMovie =  this.DetailMovie.bind(this);
  }
  async SearchMovie(search, page){
    const options = {
      uri: `${config.get('OmdapiUrl')}/`,
      qs:{
        apikey: config.get('OmdapiKey'),
        s: search,
        page: page
      },
      resolveWithFullResponse: true
    };

    let result = await rp(options);
    result = result.toJSON();
    return {
      status: result.statusCode,
      uri:result.request.uri.href, 
      body: JSON.parse(result.body),
      params: result.request.uri.path
    }
  }

  async DetailMovie(id, plot){
    const options ={
      uri: `${config.get('OmdapiUrl')}/`,
      qs:{
        i: id,
        apikey: config.get('OmdapiKey'),
        plot
      },
      resolveWithFullResponse: true
    };
    let result = await rp(options);
    result = result.toJSON();
    return {
      status: result.statusCode,
      uri:result.request.uri.href, 
      body: JSON.parse(result.body),
      params: result.request.uri.path
    }
  }
}
module.exports = new Movie();
