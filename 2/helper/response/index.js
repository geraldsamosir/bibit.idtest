const code = require('./http-status-code.json');

exports.response = (status, value, res) => {
  const state = parseInt(status, 10);
  const data = { status: state, message: code[status] };
  if (state >= 300 && state < 600) data.error = value;
  else data.value = value;
  res.status(status).json(data);
  res.end();
};

const responseValid = (status, value, res) => {
  const state = parseInt(status, 10);
  const errVal = {};
  Object.keys(value).forEach((val) => {
    const { msg, value: field } = value[val];
    const [type, msgType] = msg;
    errVal[val] = {};
    errVal[val][type] = msgType;
    errVal[val].value = field;
  });
  const data = { status: state, message: code[status], error: errVal };
  res.status(status).json(data);
  res.end();
};

exports.responseValid = (err, res, next) => {
  if (err) responseValid(422, err, res);
  else next();
};
