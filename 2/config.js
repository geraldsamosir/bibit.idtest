const nconf = require('nconf');
const path = require('path');

nconf
  .argv()
  .env([
    'ApiDocsUser',
    'ApiDocsPassword',
    'NODE_ENV',
    'PORT',
  ])
  .file({ file: path.join(__dirname, 'config.json') })
  .defaults({
    PORT: 3002,
  });

module.exports = nconf;
