const { Log_Omdaapi_url } = require('@infrastructure/database/mysql/models');

class LoggingSearchAndDetail {
  constructor(){
     this.saveLogUrl = this.saveLogUrl.bind(this);
  }
  async saveLogUrl(data, typeRequest){
    await Log_Omdaapi_url.create({
       url: data.uri,
       type_request: typeRequest,
       response: data.body,
       params: data.params
    })
  }
}

module.exports = new LoggingSearchAndDetail();