'use strict';
module.exports = {
  up:(queryInterface, Sequelize) => queryInterface.createTable('Log_Omdaapi_url', {
    ID: {
      type: Sequelize.DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true

    },
    url: {
      allowNull: false,
      type: Sequelize.STRING,
      defaultValue: 0,
    },
    params: {
      allowNull: false,
      type: Sequelize.STRING,
      defaultValue: 0
    },
    type_request: {
      allowNull:false,
      type: Sequelize.DataTypes.ENUM('Search', 'Detail')
    },
    response: {
      allowNull: false,
      type: Sequelize.JSON,
      defaultValue: null,
    },
    created_at: {
      type: 'TIMESTAMP',
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false
    },

  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('Log_Omdaapi_url')
};
