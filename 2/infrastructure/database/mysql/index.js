const config = require('@config');
const Sequelize = require('sequelize');
const fs = require('fs');
const sequelize = new Sequelize(
  config.get('MysqlDatabase'),
  config.get('MysqlUser'),
  config.get('MysqlPassword'),
  {
    host: config.get('MysqlHost'),
    port: config.get('MysqlPort'),
    dialect: 'mysql',
    operatorsAliases: Sequelize.Op,
    logging: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      // connection open 15 second
      idle: 15000
    },
  }
);



module.exports = sequelize;
