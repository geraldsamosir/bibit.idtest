require('module-alias/register');

const config = require('@config');

const db = {
  username: config.get('MysqlUser'),
  password: config.get('MysqlPassword'),
  database: config.get('MysqlDatabase'),
  host: config.get('MysqlHost'),
  dialect: 'mysql',
  timezone: '+07:00'
};

module.exports = db;

