require('module-alias/register');
const { expect } = require('chai');
const MovieUsecase = require('@usecase/movie') 


describe('function SearchMovie in Movie usecase', () => {
    it('status equal 200 ', (done) => {
        MovieUsecase.SearchMovie('Batman',2)
        .then(result=>{
            expect(result.status).to.equal(200);
            done();
        })
    })
    it('Search should be an array', (done) => {
        MovieUsecase.SearchMovie('Batman',2)
        .then(result=>{
            expect(result.body.Search).to.be.an('array')
            done();
        })
    })
})

describe('function DetailMovie  in Movie usecase', () => {
    it('status equal 200 ', (done) => {
        MovieUsecase.DetailMovie('tt4853102')
        .then(result=>{
            expect(result.status).to.equal(200);
            done();
        })
    })
    it('Search should be an Object', (done) => {
        MovieUsecase.DetailMovie('tt4853102')
        .then(result=>{
            expect(result.body).to.be.an('Object')
            done();
        })
    })
    it('Title must get it',(done)=>{
        MovieUsecase.DetailMovie('tt4853102')
        .then(result=>{
            expect(result.body.Title).to.not.equal(undefined)
            done();
        })
    })
})