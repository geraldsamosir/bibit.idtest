const app = require('@interface/webServer/server');
const supertest = require('supertest');
const server = app.listen();
const request = supertest.agent(server);

const { expect } = require('chai');
const { Log_Omdaapi_url } = require('@infrastructure/database/mysql/models');


describe('test url /search?s=Batman&page=2', () => {
    it('status equal 200 ', (done) => {
        request.get('/search?s=Batman&page=2')
        .then(result=>{
            expect(result.status).to.equal(200);
            done();
        })
    })
    it('Search should be an Object', (done) => {
        request.get('/search?s=Batman&page=2')
        .then(result=>{
            expect(result.body.value.Search).to.be.an('Array')
            done();
        })
    })
})

describe('test url /detail/tt4853102?plot=full', () => {
    it('status equal 200 ', (done) => {
        request.get('/detail/tt4853102?plot=full')
        .then(result=>{
            expect(result.status).to.equal(200);
            done();
        })
    })
    it('Search should be an Object', (done) => {
        request.get('/detail/tt4853102?plot=full')
        .then(result=>{
            expect(result.body.value).to.be.an('Object')
            done();
        })
    })
    it('Title must get it',(done)=>{
        request.get('/detail/tt4853102?plot=full')
        .then(result=>{
            expect(result.body.value.Title).to.not.equal(undefined)
            done();
        })
    })
})

// becarefull command if not to use
//clearing table after al unit test done
describe('clearing table',()=>{
  it('clearning table after done all unit test',(done)=>{
      Log_Omdaapi_url.destroy({truncate: true})
      .then(response =>{
        done();
      })
  })
});