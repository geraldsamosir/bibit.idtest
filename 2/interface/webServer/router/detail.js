const { response } = require('@helper/response/index');
const MovieUsecase = require('@usecase/movie')
const LoggingSearchAndDetailDomain = require('@domain/LoggingSearchAndDetail')


class DetailMovie {
  constructor(router){
    this.SearchMovie =  this.DetailMovie.bind(this);
    router.get('/:id', this.DetailMovie)
  }

  async DetailMovie(req, res){
    try {
    const resp =  await MovieUsecase.DetailMovie(req.params.id, req.query.plot);
    //log data to database
    LoggingSearchAndDetailDomain.saveLogUrl({
      uri:resp.uri,
      body: resp.body,
      params: resp.params
    },'Detail')
    return response(resp.status, {...resp.body}, res); 
    } catch (error) {
      return response(500, `internal server error ${error}`, res)
    }
  }
}

module.exports = (router) =>  new DetailMovie(router);
