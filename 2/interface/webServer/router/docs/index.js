const config = require('@config');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const basicAuthModule = require('basic-auth');

function basicAuth() {
  return (req, res, next) => {
    const user = basicAuthModule(req);
    if (!user || user.name !== config.get('ApiDocsUser') || user.pass !== config.get('ApiDocsPassword')) {
      res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
      res.sendStatus(401);
    } else {
      next();
    }
  };
}

// Documentation
module.exports = (router) => {
  const docTitle = config.get('ApiDocsTitle');
  const docVersion = config.get('ApiDocsVersion');

  const swaggerSpec = swaggerJSDoc({
    swaggerDefinition: {
      info: {
        title: docTitle,
        version: docVersion,
      },
      securityDefinitions: {
        ApiKeyAuth: {
          type: 'apiKey',
          name: 'Authorization',
          scheme: 'bearer',
          description: 'get Authorization from userApi key login',
          in: 'header',
        }
      }
    },
    apis: [
      './interface/webServer/router/search.yaml',
      './interface/webServer/router/detail.yaml',
    ],
  });

  const showExplorer = false;
  const options = {
    docExpansion: 'none',
  };
  const customCss = '';
  const customFavicon = '';
  const swaggerUrl = '';

  router.use(
    '/',
    basicAuth(),
    swaggerUi.serve,
    swaggerUi.setup(
      swaggerSpec,
      showExplorer,
      options,
      customCss,
      customFavicon,
      swaggerUrl,
      docTitle,
      (req, res, next) => {
        next();
      }
    )
  );
};
