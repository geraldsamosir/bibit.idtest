const { response } = require('@helper/response/index');
const MovieUsecase = require('@usecase/movie')
const LoggingSearchAndDetailDomain = require('@domain/LoggingSearchAndDetail')
class SearchMovie {
  constructor(router){
    this.SearchMovie =  this.SearchMovie.bind(this);
    router.get('/', this.SearchMovie)
  }

  async SearchMovie(req, res){
    try {     
      const resp =  await MovieUsecase.SearchMovie(req.query.s, req.query.page);
      //log data to database
      LoggingSearchAndDetailDomain.saveLogUrl({
        uri:resp.uri,
        body: resp.body,
        params: resp.params
      }, 'Search')
      return response(resp.status, {...resp.body}, res); 
    } catch (error) {
      return response(500, `internal server error ${error}`, res)
    }
  }
}

module.exports = (router) =>  new SearchMovie(router);
