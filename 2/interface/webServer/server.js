const express = require('express');
const enrouten = require('express-enrouten');
const bodyParser = require('body-parser');
const comporession = require('compression');
const  helmet = require('helmet');
const app = express();
const cors = require('cors');
const { response } = require('@helper/response/index');


app.use(cors());
app.disable('x-powered-by');

app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(helmet());
app.use(comporession());

app.use(enrouten({
  directory: './router',
}));

// Handle 404
app.use(function(req, res) {
  return response(404, {error: 'api not found'}, res);
});

// Handle 500
app.use(function(error, req, res, next) {
  return response(500, {error: 'internal server error'}, res);
});


module.exports = app;
