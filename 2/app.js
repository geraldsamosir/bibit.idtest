require('module-alias/register');
const {createTerminus} = require('@godaddy/terminus');
const http = require('http');
const app =  require('@interface/webServer/server');
const { sequelize } = require('@infrastructure/database/mysql/models');

sequelize.authenticate()
.then(() => {
    console.log('your database has connected');
})
.catch(error => {
    console.error('fail connect database');
});
const serverApi =  http.createServer(app);

createTerminus(serverApi,{
  logger: console.log,
  healthChecks: {
    '/healthz': () => Promise.resolve()
  }
}).listen(3000);
